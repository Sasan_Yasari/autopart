import logging
from math  import log, ceil

import matplotlib.pyplot as plt
import networkx as nx

epsilon = 0.0001

def log2(x):
	return log(x, 2)

def log_star(x):
	res = 0
	val = log(x, 2)
	while val > epsilon:
		res += val
		val = log(val, 2)
	return res

class Autopart:

	def __init__(self, graph, log_level=logging.ERROR):
		self.graph = graph
		self.adj_matrix = nx.adjacency_matrix(self.graph, graph.nodes()).tolil()
		self.k = 1
		self.map_g_n = {0: set(self.graph.nodes())}
		self.map_n_g = dict((n, 0) for n in self.graph.nodes())
		self.map_n_r = dict((n, idx) for idx, n in enumerate(self.graph.nodes()))
		self.step = 0

		self._recalculate_block_properties()
	
		logging.basicConfig(format="%(levelname)s: %(message)s")
		logging.getLogger().setLevel(log_level)

		self._run()

	def _recalculate_block_properties(self):
		self.w = [[self._block_weight(i, j) for j in self.groups()] for i in self.groups()]
		self.P = [[self._block_density(i, j) for j in self.groups()] for i in self.groups()]

	def _block_weight(self, group_i, group_j):
		r_from = self.group_start_idx(group_i)
		r_to   = r_from + self.group_size(group_i)
		c_from = self.group_start_idx(group_j)
		c_to   = c_from + self.group_size(group_j)
		if r_from < r_to and c_from < c_to:
			return float(sum([self.adj_matrix[r, c_from:c_to].sum() for r in range(r_from, r_to)]))
		else:
			return 0

	def nodes(self):
		return self.graph.nodes()

	def clusters(self):
		return self.map_g_n

	def groups(self):
		return range(self.k)

	def _block_density(self, group_i, group_j):
		return ( self.block_weight(group_i, group_j) + .5 ) / ( self.block_size(group_i, group_j) + 1 )

	def block_weight(self, group_i, group_j):
		return self.w[group_i][group_j]

	def block_size(self, group_i, group_j):
		return self.group_size(group_i) * self.group_size(group_j)

	def group_start_idx(self, group_i):
		return sum([self.group_size(g) for g in range(group_i)])
	
	def group_size(self, group_i):
		return len(self.map_g_n[group_i])

	def group_sizes(self):
		return [len(self.map_g_n[g]) for g in self.map_g_n]

	def description_cost(self):
		cost = log_star(self.k)
		cost += self.description_cost_group_sizes()
		cost += self.description_cost_block_weights()
		return cost

	def description_cost_group_sizes(self):
		if self.k == 1:
			return 0
		else:
			sizes = sorted([self.group_size(grp) for grp in self.groups()], reverse=True)
			def a(group_i):
				val = 1 - self.k + group_i
				for g in range(group_i, self.k):
					val += sizes[g]
				return val
			res = 0
			for grp in range(self.k - 1):
				res += ceil(log2(a(grp)))
			return res

	def description_cost_block_weights(self):
		return sum((ceil(log2(self.block_size(i, j) + 1)) for i in self.groups() for j in self.groups()))

	def code_cost(self):
		return sum((self.block_code_cost(i, j) for i in self.groups() for j in self.groups()))

	def block_code_cost(self, group_i, group_j):
		i, j = group_i, group_j
		cost = 0
		cost -= self.block_weight(i, j) * log2(self.block_density(i, j))
		cost -= (self.block_size(i, j) - self.block_weight(i, j)) * log2(1 - self.block_density(i, j))
		return cost

	def block_size(self, group_i, group_j):
		return self.group_size(group_i) * self.group_size(group_j)


	def total_cost(self):
		return self.description_cost() + self.code_cost()

	def block_density(self, group_i, group_j):
		return self.P[group_i][group_j]

	def group_entropy_per_node(self, group_i):
		if self.group_size(group_i) == 0:
			return 0
		else:
			entropy = sum((self.block_code_cost(group_i, j) + self.block_code_cost(j, group_i) for j in self.groups()))
			return entropy / self.group_size(group_i)

	def _add_new_group(self):
		new_group = self.k
		self.map_g_n[new_group] = set()
		self.k += 1
		self._recalculate_block_properties()

	def _move_node_to_new_group(self, node):
		self.map_g_n[self.map_n_g[node]].remove(node)
		self.map_g_n[self.k - 1].add(node)
		self.map_n_g[node] = self.k - 1

		self._rearrange_matrix_and_mappings(self.map_g_n, self.map_n_g)
		self._recalculate_block_properties()


	def group_entropy_per_node_exclude(self, group_i, node):
		if self.group_size(group_i) == 1 or self.group_size(group_i) == 0:
			return 0

		def P(w, n):
			return (w + 0.5) / (n + 1)

		x = self.map_n_r[node]
		entropy = 0
		for j in self.groups():
			n_rj = n_jr = w_rj = w_jr = p_rj = p_jr = 0
			if j == group_i:
				if self.group_size(group_i) > 1:
					n_rj = n_jr = (self.group_size(j) - 1) * (self.group_size(j) - 1)
					w_rj = w_jr = self.block_weight(j, j) - self.row_weight(x, group_i) - self.col_weight(x, group_i) + self.cell(x, x)
					p_rj = p_jr = P(w_rj, n_rj)
			elif j == self.k - 1:
				if self.group_size(group_i) > 1:
					n_rj = n_jr = (self.group_size(group_i) - 1) * (self.group_size(j) + 1)
					w_rj = self.block_weight(group_i, j) - self.row_weight(x, j) + self.col_weight(x, group_i)
					w_jr = self.block_weight(j, group_i) - self.col_weight(x, j) + self.row_weight(x, group_i)
					p_rj = P(w_rj, n_rj)
					p_jr = P(w_jr, n_jr)
			else:
				if self.group_size(group_i) > 1 and self.group_size(j) > 0:
					n_rj = n_jr = (self.group_size(group_i) - 1) * self.group_size(j)
					w_rj = self.block_weight(group_i, j) - self.row_weight(x, j)
					w_jr = self.block_weight(j, group_i) - self.col_weight(x, j)
					p_rj = P(w_rj, n_rj)
					p_jr = P(w_jr, n_jr)
			entropy -= w_rj * log2(p_rj) + (n_rj - w_rj) * log2(1 - p_rj)
			entropy -= w_jr * log2(p_jr) + (n_jr - w_jr) * log2(1 - p_jr)
		entropy /= self.group_size(group_i) - 1
		return entropy

	def row_weight(self, row, group_i):
		c_from = self.group_start_idx(group_i)
		c_to   = c_from + self.group_size(group_i)
		if c_from < c_to:
			return float(self.adj_matrix[row, c_from:c_to].sum())
		else:
			return 0

	def col_weight(self, col, group_i):
		r_from = self.group_start_idx(group_i)
		r_to   = r_from + self.group_size(group_i)
		if r_from < r_to:
			return float(self.adj_matrix[r_from:r_to, col].sum())
		else:
			return 0

	def cell(self, row, col):
		return float(self.adj_matrix[row, col])

	def _report_adj_matrix(self, loop_name, it_num):
		if logging.getLogger().getEffectiveLevel() <= logging.INFO:
			self._plot_adj_matrix()
			plt.savefig('plot/autopart_step_' + str(self.step) + '_' + loop_name + '_' + str(it_num))
		self.step += 1

	def _plot_adj_matrix(self):
		plt.matshow(self.adj_matrix.todense(), cmap=plt.cm.Greys)
		for g in self.groups():
			idx = self.group_start_idx(g)
			if idx > 0:
				plt.axvline(idx, color='#ff9933', lw=2)
				plt.axhline(idx, color='#ff9933', lw=2)

	def show_result(self):
		self._plot_adj_matrix()
		plt.show()

	def _rearrange_matrix_and_mappings(self, map_g_n, map_n_g):
		assert(len(map_n_g) == sum([len(map_g_n[g]) for g in map_g_n]))
		order_node = [node for group in map_g_n for node in map_g_n[group]]
		order_row  = [self.map_n_r[node] for group in map_g_n for node in map_g_n[group]]
        
		adj_matrix = self.adj_matrix
		temp = {}
		used = set()
		for idx, row_num in enumerate(order_row):
			if idx == row_num:
				used.add(row_num)
				continue
			else:
				if idx not in used:
					temp[idx] = adj_matrix[idx, :]
				if row_num in temp:
					adj_matrix[idx, :] = temp[row_num]
				else:
					adj_matrix[idx, :] = adj_matrix[row_num, :]
				used.add(row_num)
		assert len(used) == self.graph.order()
		temp.clear()
		used.clear()
		for idx, col_num in enumerate(order_row):
			if idx == col_num:
				used.add(col_num)
				continue
			else:
				if idx not in used:
					temp[idx] = adj_matrix[:, idx]
				if col_num in temp:
					adj_matrix[:, idx] = temp[col_num]
				else:
					adj_matrix[:, idx] = adj_matrix[:, col_num]
				used.add(col_num)

		assert len(used) == self.graph.order()
		self.map_g_n = map_g_n
		self.map_n_g = map_n_g
		self.map_n_r = dict((node, idx) for idx, node in enumerate(order_node))

		self._recalculate_block_properties()


	def _inner_loop(self):
		inner_loop_it = 0
		while True:
			map_n_g = {}
			map_g_n = dict((g, set()) for g in self.groups())
			for node in self.nodes():
				next_grp = min(self.groups(), key=lambda g: self.rearrange_cost(node, g))
				map_n_g[node] = next_grp
				map_g_n[next_grp].add(node)
				logging.debug('Move node %s from group %d to %d', node, self.map_n_g[node], next_grp)

			prev_total_cost = self.total_cost()
			self._rearrange_matrix_and_mappings(map_g_n, map_n_g)
            
			logging.info('After inner optimization %s', self.group_sizes())

			self._report_adj_matrix('inner', inner_loop_it)
			if prev_total_cost - self.total_cost() < epsilon:
				break
			else:
				inner_loop_it += 1
				logging.debug('Iteration inner %d', inner_loop_it)

	def rearrange_cost(self, node, next_group):
		g = self.map_n_g[node]
		x = self.map_n_r[node]
		i = next_group
		cost = 0
		for j in self.groups():
			cost -= self.row_weight(x, j) * log2(self.block_density(i, j)) + (self.group_size(j) - self.row_weight(x, j)) * log2(1 - self.block_density(i, j))
			cost -= self.col_weight(x, j) * log2(self.block_density(j, i)) + (self.group_size(j) - self.col_weight(x, j)) * log2(1 - self.block_density(j, i))
			cost += self.cell(x, x) * (log2(self.block_density(i, g)) + log2(self.block_density(g, i)) - log2(self.block_density(i, i)))
			cost += (1 - self.cell(x, x)) * (log2(1 - self.block_density(i, g)) + log2(1 - self.block_density(g, i)) - log2(1 - self.block_density(i, i)))
		return cost

	def _report_code_cost(self):
		logging.debug('Step %d: Code cost = %f', self.step, self.code_cost())

	def _run(self):
		outer_loop_it = 0
		while True:
			prev_total_cost = self.total_cost()
			group_r = max(self.groups(), key=lambda g: self.group_entropy_per_node(g))
			self._add_new_group()

			for node in list((self.map_g_n[group_r])):
			    if self.group_entropy_per_node_exclude(group_r, node) < self.group_entropy_per_node(group_r):
			        self._move_node_to_new_group(node)

			logging.info("After splitting: %s", self.group_sizes())
			self._report_adj_matrix('outer', outer_loop_it)
			self._inner_loop()
			self._report_code_cost()
			if prev_total_cost - self.total_cost() < epsilon:
				break
			else:
				outer_loop_it += 1
				logging.debug("Iteration outer %d", outer_loop_it)



	def outlier_score(self, group_i, group_j):
		if self.w[group_i][group_j] == 0:
			return 0
		else:
			w_orig, P_orig = self.w[group_i][group_j], self.P[group_i][group_j]
			cost = self.total_cost()
			self.w[group_i][group_j] -= 1
			self.P[group_i][group_j] = self.w[group_i][group_j] / self.block_size(group_i, group_j)
			score = cost - self.total_cost()
			self.w[group_i][group_j], self.P[group_i][group_j] = w_orig, P_orig
			return score



gr = nx.read_graphml('gnet.graphml')

Autopart(gr, log_level=logging.DEBUG)

