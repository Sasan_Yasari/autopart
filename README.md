# Autopart Definition
Parameter-Free Graph Partitioning and Outlier Detection

# Goal
Clustering graph into optimum K parts and find outlier edges (between clusters).

# Proposed method:
## Solved the problem by a two step iterative process:
```bash
1. Efficiently search for the best node grouping k (k = 1, 2, . . .) (outerloop)
2. For a given k, find a good arrangement G. (innerloop)
```

# Complexity:
```bash
1. The complexity of InnerLoop is O(w(D) · k · I) where I is the number of iterations.
2. The outerLoop algorithm is run k∗ times, so the overall complexity of the search
is O( w(D) · (k∗)^2 · I).
```
# Output:
Graph adjacency matrix with clustering. Results are in the ​plot​ folder.
